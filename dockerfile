FROM python
RUN pip3 install flask && pip3 install torch torchvision --extra-index-url https://download.pytorch.org/whl/cpu
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y
COPY requirements.txt /
RUN pip3 install -r /requirements.txt
COPY . /
EXPOSE 5000
CMD ["python","/Server.py"]

